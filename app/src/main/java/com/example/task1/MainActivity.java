package com.example.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView text_view;
    private Button button;
    private EditText edit_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_view = findViewById(R.id.text_view);
        button = findViewById(R.id.button);
        edit_text = findViewById(R.id.edit_text);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "message!!!", Toast.LENGTH_LONG).show();

                String income_data = edit_text.getText().toString().trim();
                income_data = income_data.replace(" ", "");

                int[] price;
                int discount = 0;
                int offset = 0;
                int readLength = 0;
                int[] data_for_answer = null;
                boolean check = true;
                int[] data;
                try {
                    String[] data_0 = income_data.split(",");
                    data = new int[data_0.length];
                    for (int i = 0; i < data.length; i++) {
                        data[i] = Integer.parseInt(data_0[i]);
                    }

                    price = new int[data.length-3];
                    for (int i = 0; i < data.length; i++)
                    {
                        if (i==data.length-1)
                        {
                            readLength = data[i];
                        }
                        else if (i==data.length-2)
                        {
                            offset = data[i];
                        }
                        else if (i==data.length-3)
                        {
                            discount = data[i];
                        }
                        else
                        {
                            price[i] = data[i];
                        }

                    }

                    data_for_answer = decryptData(price, discount, offset, readLength);

                }
                catch (Exception exc)
                {
                    check = false;
                }

                String answer = "";
                    if (data_for_answer != null) {

                        answer += "[";
                        for (int i = 0; i < data_for_answer.length; i++) {
                            answer += data_for_answer[i];
                            if (i!=data_for_answer.length-1) {
                                answer += ",";
                            }
                        }
                        answer += "]";
                    } else {
                        answer += "некорректные данные,\n для тестирования введите '5,100,20,66,16,50,1,3'";
                    }

                    text_view.setText(answer);
            }
        });
    }

    public int[] decryptData( int[] price,
                              int discount,
                              int offset,
                              int readLength)
    {
        //TODO реализовать метод
        int[] result;

        boolean check_price = true;
        for (int i = 0; i < price.length; i++)
        {
            if (price[i] <= 0 || price[i] > 21474836)
            {
                check_price = false;
            }
        }

        boolean check_discount = true;
        if (!(1<=discount && discount<=99))
        {
            check_discount = false;
        }

        boolean check_offset = true;
        if (offset < 0 || offset>=price.length)
        {
            check_offset = false;
        }

        boolean check_readLength = true;
        if (readLength < 0 || offset+readLength>=price.length)
        {
            check_readLength = false;
        }

        if (check_price && check_discount && check_offset && check_readLength)
        {
            result = new int[readLength];

            for(int i =0; i<result.length; i++)
            {
                result[i] = price[offset+i] * discount / 100;
            }

            return result;
        }
        else
        {
            return null;
        }
    }

}


